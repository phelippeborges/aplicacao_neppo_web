-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Aplicacao_Neppo
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Aplicacao_Neppo
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Aplicacao_Neppo` DEFAULT CHARACTER SET latin1 ;
USE `Aplicacao_Neppo` ;

-- -----------------------------------------------------
-- Table `Aplicacao_Neppo`.`Colaboradores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Aplicacao_Neppo`.`Colaboradores` (
  `id_colab` INT(11) NOT NULL AUTO_INCREMENT,
  `Username` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_colab`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Aplicacao_Neppo`.`Cursos_livros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Aplicacao_Neppo`.`Cursos_livros` (
  `id_curso` INT(11) NOT NULL AUTO_INCREMENT,
  `nome_cur` VARCHAR(45) NOT NULL,
  `carga_horaria` INT(11) NOT NULL,
  `nivel_cur` INT(11) NOT NULL,
  `certificado` MEDIUMBLOB NULL DEFAULT NULL,
  `data` DATE NULL DEFAULT NULL,
  `pontuacao_cur` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_curso`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Aplicacao_Neppo`.`Colaboradores_Cursos_livros`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Aplicacao_Neppo`.`Colaboradores_Cursos_livros` (
  `ID_colab` INT(11) NULL DEFAULT NULL,
  `ID_cur` INT(11) NULL DEFAULT NULL,
  INDEX `fk_id_colab_idx` (`ID_colab` ASC) VISIBLE,
  INDEX `fk_id_cur_idx` (`ID_cur` ASC) VISIBLE,
  CONSTRAINT `fk_id_colab`
    FOREIGN KEY (`ID_colab`)
    REFERENCES `Aplicacao_Neppo`.`Colaboradores` (`id_colab`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_cur`
    FOREIGN KEY (`ID_cur`)
    REFERENCES `Aplicacao_Neppo`.`Cursos_livros` (`id_curso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Aplicacao_Neppo`.`Desempenho_Colaboradores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Aplicacao_Neppo`.`Desempenho_Colaboradores` (
  `pontuacao_total` INT(11) NOT NULL,
  `ID_colab` INT(11) NULL DEFAULT NULL,
  `level_colab` INT(11) NULL DEFAULT NULL,
  `neppo_money` DECIMAL(6,2) NULL DEFAULT NULL,
  PRIMARY KEY (`pontuacao_total`),
  INDEX `fk_id_colab_des_idx` (`ID_colab` ASC) VISIBLE,
  CONSTRAINT `fk_id_colab_des`
    FOREIGN KEY (`ID_colab`)
    REFERENCES `Aplicacao_Neppo`.`Colaboradores` (`id_colab`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
