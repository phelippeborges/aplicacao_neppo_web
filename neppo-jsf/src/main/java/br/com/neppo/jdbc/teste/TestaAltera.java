package br.com.neppo.jdbc.teste;

import br.com.neppo.jdbc.dao.ColaboradorDao;
import br.com.neppo.jdbc.model.Colaborador;

public class TestaAltera {
    public TestaAltera() {
    }

    public static void main(String[] args) {
        Colaborador colaborador = new Colaborador();
        colaborador.setUsername("Teste3");
        colaborador.setPassword("senha2");
        colaborador.setUsername("Teste2");
        ColaboradorDao dao = new ColaboradorDao();
        dao.altera(colaborador);
        System.out.println("Colaborador Alterado!");
    }
}
