package br.com.neppo.jdbc.dao;

import br.com.neppo.jdbc.ConnectionFactory;
import br.com.neppo.jdbc.model.Colaborador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ColaboradorDao {
    private Connection connection = (new ConnectionFactory()).getConnection();

    public ColaboradorDao() {
    }

    public void adiciona(Colaborador colaborador) {
        String sql = "insert into Colaboradores (Username, Password)values (?,?)";

        try {
            PreparedStatement stmt = this.connection.prepareStatement(sql);
            stmt.setString(1, colaborador.getUsername());
            stmt.setString(2, colaborador.getPassword());
            stmt.execute();
            stmt.close();
        } catch (SQLException var4) {
            throw new RuntimeException(var4);
        }
    }

    public List<Colaborador> getLista() {
        try {
            List<Colaborador> colaboradores = new ArrayList();
            PreparedStatement stmt = this.connection.prepareStatement("select * from Colaboradores");
            ResultSet rs = stmt.executeQuery();

            while(rs.next()) {
                Colaborador colaborador = new Colaborador();
                colaborador.setId_colab(rs.getInt("id_colab"));
                colaborador.setUsername(rs.getString("Username"));
                colaborador.setPassword(rs.getString("Password"));
                colaboradores.add(colaborador);
            }

            rs.close();
            stmt.close();
            return colaboradores;
        } catch (SQLException var5) {
            throw new RuntimeException(var5);
        }
    }

    public void altera(Colaborador colaborador) {
        String sql = "update Colaboradores set Username=?, Password=? where Username=?";

        try {
            PreparedStatement stmt = this.connection.prepareStatement(sql);
            stmt.setString(1, colaborador.getUsername());
            stmt.setString(2, colaborador.getPassword());
            stmt.setString(3, colaborador.getUsername());
            stmt.execute();
            stmt.close();
        } catch (SQLException var4) {
            throw new RuntimeException(var4);
        }
    }

    public void remove(Colaborador colaborador) {
        try {
            PreparedStatement stmt = this.connection.prepareStatement("delete from Colaboradores where Username=?");
            stmt.setString(1, colaborador.getUsername());
            stmt.execute();
            stmt.close();
        } catch (SQLException var3) {
            throw new RuntimeException(var3);
        }
    }
    public Colaborador getColab(Colaborador colaborador) {

        try {
            PreparedStatement stmt = this.connection.prepareStatement("select * from Colaboradores where id_colab=?");
            stmt.setInt(1, colaborador.getId_colab());
            ResultSet rs = stmt.executeQuery();


            Colaborador asd = new Colaborador();
            while(rs.next()) {
                Colaborador colab = new Colaborador();
                colab.setId_colab(rs.getInt("id_colab"));
                colab.setUsername(rs.getString("Username"));
                colab.setPassword(rs.getString("Password"));
                asd = colab;
            }
            rs.close();
            stmt.close();
            return asd;
        } catch (SQLException var5) {
            throw new RuntimeException(var5);
        }
    }
}
