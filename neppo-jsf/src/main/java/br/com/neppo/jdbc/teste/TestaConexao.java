package br.com.neppo.jdbc.teste;

import br.com.neppo.jdbc.ConnectionFactory;
import java.sql.Connection;
import java.sql.SQLException;

public class TestaConexao {
    public TestaConexao() {
    }

    public static void main(String[] args) throws SQLException {
        Connection connection = (new ConnectionFactory()).getConnection();
        System.out.println("Conexão aberta!");
        connection.close();
    }
}
