package br.com.neppo.jdbc.model;

public class Desempenho {
    private int pontuacao_total;
    private int ID_colab;
    private int level_colab;
    private  double neppo_money;


    public Desempenho(int pontuacao_total, int ID_colab, int level_colab, double neppo_money) {
        this.pontuacao_total = pontuacao_total;
        this.ID_colab = ID_colab;
        this.level_colab = level_colab;
        this.neppo_money = neppo_money;

    }

    public int getPontuacao_total() {
        return pontuacao_total;
    }

    public void setPontuacao_total(int pontuacao_total) {
        this.pontuacao_total = pontuacao_total;
    }

    public int getID_colab() {
        return ID_colab;
    }

    public void setID_colab(int ID_colab) {
        this.ID_colab = ID_colab;
    }

    public int getLevel_colab() {
        return level_colab;
    }

    public void setLevel_colab(int level_colab) {
        this.level_colab = level_colab;
    }

    public double getNeppo_money() {
        return neppo_money;
    }

    public void setNeppo_money(double neppo_money) {
        this.neppo_money = neppo_money;
    }


}
