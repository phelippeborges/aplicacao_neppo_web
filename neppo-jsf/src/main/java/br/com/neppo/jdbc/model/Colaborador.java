package br.com.neppo.jdbc.model;

public class Colaborador {
    private int id_colab;
    private String username;
    private String password;



    public Colaborador(int id_colab, String username, String password) {
        this.id_colab = id_colab;
        this.username = username;
        this.password = password;
    }

    public int getId_colab() {
        return this.id_colab;
    }

    public void setId_colab(int id_colab) {
        this.id_colab = id_colab;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    //testesssssssssssssssssssss pro GIT
}
