package br.com.neppo.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    public ConnectionFactory() {
    }

    public Connection getConnection() {
        System.out.println("Conectando ao banco...");

        try {
            return DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/Aplicacao_Neppo", "grupo12", "121212");
        } catch (SQLException var2) {
            throw new RuntimeException(var2);
        }
    }
}
