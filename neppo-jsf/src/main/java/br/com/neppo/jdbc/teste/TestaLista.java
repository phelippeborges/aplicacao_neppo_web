package br.com.neppo.jdbc.teste;

import br.com.neppo.jdbc.dao.ColaboradorDao;
import br.com.neppo.jdbc.model.Colaborador;
import java.util.Iterator;
import java.util.List;

public class TestaLista {
    public TestaLista() {
    }

    public static void main(String[] args) {
        ColaboradorDao dao = new ColaboradorDao();
        List<Colaborador> colaboradores = dao.getLista();
        Iterator var3 = colaboradores.iterator();

        while(var3.hasNext()) {
            Colaborador colaborador = (Colaborador)var3.next();
            System.out.println("ID: " + colaborador.getId_colab());
            System.out.println("Username: " + colaborador.getUsername());
            System.out.println("Password: " + colaborador.getPassword());
        }

    }
}
