package br.com.neppo.jdbc.teste;

import br.com.neppo.jdbc.dao.ColaboradorDao;
import br.com.neppo.jdbc.model.Colaborador;

public class TestaGetColab {

    private static Colaborador frota = new Colaborador(11,"Frota","333333");
    public TestaGetColab() {
    }

    public static void main(String[] args) {
        ColaboradorDao dao = new ColaboradorDao();
        Colaborador colab = dao.getColab(frota);

        System.out.println("ID: " + colab.getId_colab());
        System.out.println("Username: " + colab.getUsername());
        System.out.println("Password: " + colab.getPassword());
    }

}
