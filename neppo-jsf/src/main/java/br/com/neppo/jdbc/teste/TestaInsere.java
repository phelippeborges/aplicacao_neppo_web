package br.com.neppo.jdbc.teste;

import br.com.neppo.jdbc.dao.ColaboradorDao;
import br.com.neppo.jdbc.model.Colaborador;

public class TestaInsere {
    public TestaInsere() {
    }

    public static void main(String[] args) {
        Colaborador colaborador = new Colaborador();
        colaborador.setUsername("Teste");
        colaborador.setPassword("senha");
        ColaboradorDao dao = new ColaboradorDao();
        dao.adiciona(colaborador);
        System.out.println("Colaborador gravado!");
    }
}
