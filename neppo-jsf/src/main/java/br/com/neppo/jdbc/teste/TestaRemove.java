package br.com.neppo.jdbc.teste;


import br.com.neppo.jdbc.dao.ColaboradorDao;
import br.com.neppo.jdbc.model.Colaborador;

public class TestaRemove {
    public TestaRemove() {
    }

    public static void main(String[] args) {
        Colaborador colaborador = new Colaborador();
        colaborador.setUsername("Teste");
        ColaboradorDao dao = new ColaboradorDao();
        dao.remove(colaborador);
        System.out.println("Colaborador Removido!");
    }
}
