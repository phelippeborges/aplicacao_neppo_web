package br.com.neppo.servlet;

import br.com.neppo.jdbc.dao.ColaboradorDao;
import br.com.neppo.jdbc.model.Colaborador;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/add/*")
public class AdicionaColaboradorServlet extends HttpServlet{
    public AdicionaColaboradorServlet() {
    }

    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        PrintWriter out = response.getWriter();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Colaborador colaborador = new Colaborador();
        colaborador.setUsername(username);
        colaborador.setPassword(password);
        ColaboradorDao dao = new ColaboradorDao();
        dao.adiciona(colaborador);
        out.println("<html>");
        out.println("<body>");
        out.println("Colaborador " + colaborador.getUsername() + " adicionado com sucesso!");
        out.println("</body>");
        out.println("</html>");
    }
}
