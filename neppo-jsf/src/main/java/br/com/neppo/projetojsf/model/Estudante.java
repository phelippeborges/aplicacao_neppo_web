package br.com.neppo.projetojsf.model;

import br.com.neppo.projetojsf.model.enums.Turno;

public class Estudante {
    private String nome = "Eric";
    private String sobrenome = "Sardela";
    private double nota1 = 10.0D;
    private double nota2;
    private Turno turno;
    private double nota3;

    public Estudante() {
        this.turno = Turno.MATUTINO;
        this.nota3 = 10.0D;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return this.sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public double getNota1() {
        return this.nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

    public double getNota2() {
        return this.nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    public double getNota3() {
        return this.nota3;
    }

    public void setNota3(double nota3) {
        this.nota3 = nota3;
    }

    public Turno getTurno() {
        return this.turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }
}
