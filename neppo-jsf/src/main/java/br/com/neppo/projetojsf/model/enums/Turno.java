package br.com.neppo.projetojsf.model.enums;

public enum Turno {
    MATUTINO,
    VESPERTINO,
    NOTURNO;

    private Turno() {
    }
}
