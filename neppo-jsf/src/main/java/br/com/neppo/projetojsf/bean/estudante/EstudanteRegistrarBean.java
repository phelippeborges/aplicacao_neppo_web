package br.com.neppo.projetojsf.bean.estudante;

import br.com.neppo.projetojsf.model.Estudante;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import javax.inject.Named;


@Named
public class EstudanteRegistrarBean implements Serializable {
    private Estudante estudante = new Estudante();
    private String[] nomesArray = new String[]{"Dev", "eh", "massa"};
    private List<String> nomesList = Arrays.asList("Esta", "eh", "uma", "lista");
    private Set<String> nomesSet = new HashSet(Arrays.asList("Goku", "Naruto", "Saitama", "Midoriya"));
    private Map<String, String> nomesMap = new HashMap();

    public EstudanteRegistrarBean() {
        this.nomesMap.put("Goku", "O segundo mais forte");
        this.nomesMap.put("Naruto", "O mais fraco");
        this.nomesMap.put("Saitama", "O mais forte");
        this.nomesMap.put("Midoriya", "O terceiro mais forte");
        Iterator var1 = this.nomesMap.entrySet().iterator();

        while(var1.hasNext()) {
            Entry<String, String> entry = (Entry)var1.next();
            System.out.println((String)entry.getKey());
            System.out.println((String)entry.getValue());
        }

    }

    public Map<String, String> getNomesMap() {
        return this.nomesMap;
    }

    public void setNomesMap(Map<String, String> nomesMap) {
        this.nomesMap = nomesMap;
    }

    public Set<String> getNomesSet() {
        return this.nomesSet;
    }

    public void setNomesSet(Set<String> nomesSet) {
        this.nomesSet = nomesSet;
    }

    public List<String> getNomesList() {
        return this.nomesList;
    }

    public void setNomesList(List<String> nomesList) {
        this.nomesList = nomesList;
    }

    public String[] getNomesArray() {
        return this.nomesArray;
    }

    public void setNomesArray(String[] nomesArray) {
        this.nomesArray = nomesArray;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }
}
